defmodule DigitalMenuServicesWeb.InternalControllerTest do
  use DigitalMenuServicesWeb.ConnCase

  alias DigitalMenuServices.Credential
  alias DigitalMenuServices.Credential.Internal

  @create_attrs %{
    password: "some password",
    type: "some type"
  }
  @update_attrs %{
    password: "some updated password",
    type: "some updated type"
  }
  @invalid_attrs %{password: nil, type: nil}

  def fixture(:internal) do
    {:ok, internal} = Credential.create_internal(@create_attrs)
    internal
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all internals", %{conn: conn} do
      conn = get(conn, Routes.internal_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create internal" do
    test "renders internal when data is valid", %{conn: conn} do
      conn = post(conn, Routes.internal_path(conn, :create), internal: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.internal_path(conn, :show, id))

      assert %{
               "id" => id,
               "password" => "some password",
               "type" => "some type"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.internal_path(conn, :create), internal: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update internal" do
    setup [:create_internal]

    test "renders internal when data is valid", %{conn: conn, internal: %Internal{id: id} = internal} do
      conn = put(conn, Routes.internal_path(conn, :update, internal), internal: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.internal_path(conn, :show, id))

      assert %{
               "id" => id,
               "password" => "some updated password",
               "type" => "some updated type"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, internal: internal} do
      conn = put(conn, Routes.internal_path(conn, :update, internal), internal: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete internal" do
    setup [:create_internal]

    test "deletes chosen internal", %{conn: conn, internal: internal} do
      conn = delete(conn, Routes.internal_path(conn, :delete, internal))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.internal_path(conn, :show, internal))
      end
    end
  end

  defp create_internal(_) do
    internal = fixture(:internal)
    %{internal: internal}
  end
end

defmodule DigitalMenuServicesWeb.DishesControllerTest do
  use DigitalMenuServicesWeb.ConnCase

  alias DigitalMenuServices.Menu
  alias DigitalMenuServices.Menu.Dishes

  @create_attrs %{
    description: "some description",
    enabled: "some enabled",
    extras: [],
    image: "some image",
    name: "some name",
    price: "some price"
  }
  @update_attrs %{
    description: "some updated description",
    enabled: "some updated enabled",
    extras: [],
    image: "some updated image",
    name: "some updated name",
    price: "some updated price"
  }
  @invalid_attrs %{description: nil, enabled: nil, extras: nil, image: nil, name: nil, price: nil}

  def fixture(:dishes) do
    {:ok, dishes} = Menu.create_dishes(@create_attrs)
    dishes
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all dishes", %{conn: conn} do
      conn = get(conn, Routes.dishes_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create dishes" do
    test "renders dishes when data is valid", %{conn: conn} do
      conn = post(conn, Routes.dishes_path(conn, :create), dishes: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.dishes_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some description",
               "enabled" => "some enabled",
               "extras" => [],
               "image" => "some image",
               "name" => "some name",
               "price" => "some price"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.dishes_path(conn, :create), dishes: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update dishes" do
    setup [:create_dishes]

    test "renders dishes when data is valid", %{conn: conn, dishes: %Dishes{id: id} = dishes} do
      conn = put(conn, Routes.dishes_path(conn, :update, dishes), dishes: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.dishes_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some updated description",
               "enabled" => "some updated enabled",
               "extras" => [],
               "image" => "some updated image",
               "name" => "some updated name",
               "price" => "some updated price"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, dishes: dishes} do
      conn = put(conn, Routes.dishes_path(conn, :update, dishes), dishes: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete dishes" do
    setup [:create_dishes]

    test "deletes chosen dishes", %{conn: conn, dishes: dishes} do
      conn = delete(conn, Routes.dishes_path(conn, :delete, dishes))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.dishes_path(conn, :show, dishes))
      end
    end
  end

  defp create_dishes(_) do
    dishes = fixture(:dishes)
    %{dishes: dishes}
  end
end

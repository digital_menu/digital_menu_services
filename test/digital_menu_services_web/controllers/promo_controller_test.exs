defmodule DigitalMenuServicesWeb.PromoControllerTest do
  use DigitalMenuServicesWeb.ConnCase

  alias DigitalMenuServices.Restaurant
  alias DigitalMenuServices.Restaurant.Promo

  @create_attrs %{
    begin_date: "some begin_date",
    description: "some description",
    end_date: "some end_date",
    image: "some image",
    name: "some name",
    type: "some type"
  }
  @update_attrs %{
    begin_date: "some updated begin_date",
    description: "some updated description",
    end_date: "some updated end_date",
    image: "some updated image",
    name: "some updated name",
    type: "some updated type"
  }
  @invalid_attrs %{begin_date: nil, description: nil, end_date: nil, image: nil, name: nil, type: nil}

  def fixture(:promo) do
    {:ok, promo} = Restaurant.create_promo(@create_attrs)
    promo
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all promos", %{conn: conn} do
      conn = get(conn, Routes.promo_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create promo" do
    test "renders promo when data is valid", %{conn: conn} do
      conn = post(conn, Routes.promo_path(conn, :create), promo: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.promo_path(conn, :show, id))

      assert %{
               "id" => id,
               "begin_date" => "some begin_date",
               "description" => "some description",
               "end_date" => "some end_date",
               "image" => "some image",
               "name" => "some name",
               "type" => "some type"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.promo_path(conn, :create), promo: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update promo" do
    setup [:create_promo]

    test "renders promo when data is valid", %{conn: conn, promo: %Promo{id: id} = promo} do
      conn = put(conn, Routes.promo_path(conn, :update, promo), promo: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.promo_path(conn, :show, id))

      assert %{
               "id" => id,
               "begin_date" => "some updated begin_date",
               "description" => "some updated description",
               "end_date" => "some updated end_date",
               "image" => "some updated image",
               "name" => "some updated name",
               "type" => "some updated type"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, promo: promo} do
      conn = put(conn, Routes.promo_path(conn, :update, promo), promo: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete promo" do
    setup [:create_promo]

    test "deletes chosen promo", %{conn: conn, promo: promo} do
      conn = delete(conn, Routes.promo_path(conn, :delete, promo))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.promo_path(conn, :show, promo))
      end
    end
  end

  defp create_promo(_) do
    promo = fixture(:promo)
    %{promo: promo}
  end
end

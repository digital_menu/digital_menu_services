defmodule DigitalMenuServicesWeb.SectionControllerTest do
  use DigitalMenuServicesWeb.ConnCase

  alias DigitalMenuServices.Menu
  alias DigitalMenuServices.Menu.Section

  @create_attrs %{
    back_image: "some back_image",
    description: "some description",
    name: "some name"
  }
  @update_attrs %{
    back_image: "some updated back_image",
    description: "some updated description",
    name: "some updated name"
  }
  @invalid_attrs %{back_image: nil, description: nil, name: nil}

  def fixture(:section) do
    {:ok, section} = Menu.create_section(@create_attrs)
    section
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all sections", %{conn: conn} do
      conn = get(conn, Routes.section_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create section" do
    test "renders section when data is valid", %{conn: conn} do
      conn = post(conn, Routes.section_path(conn, :create), section: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.section_path(conn, :show, id))

      assert %{
               "id" => id,
               "back_image" => "some back_image",
               "description" => "some description",
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.section_path(conn, :create), section: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update section" do
    setup [:create_section]

    test "renders section when data is valid", %{conn: conn, section: %Section{id: id} = section} do
      conn = put(conn, Routes.section_path(conn, :update, section), section: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.section_path(conn, :show, id))

      assert %{
               "id" => id,
               "back_image" => "some updated back_image",
               "description" => "some updated description",
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, section: section} do
      conn = put(conn, Routes.section_path(conn, :update, section), section: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete section" do
    setup [:create_section]

    test "deletes chosen section", %{conn: conn, section: section} do
      conn = delete(conn, Routes.section_path(conn, :delete, section))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.section_path(conn, :show, section))
      end
    end
  end

  defp create_section(_) do
    section = fixture(:section)
    %{section: section}
  end
end

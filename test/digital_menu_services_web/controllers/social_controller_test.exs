defmodule DigitalMenuServicesWeb.SocialControllerTest do
  use DigitalMenuServicesWeb.ConnCase

  alias DigitalMenuServices.Credential
  alias DigitalMenuServices.Credential.Social

  @create_attrs %{
    access_token: "some access_token",
    refresh_token: "some refresh_token",
    social_media: "some social_media",
    type: "some type"
  }
  @update_attrs %{
    access_token: "some updated access_token",
    refresh_token: "some updated refresh_token",
    social_media: "some updated social_media",
    type: "some updated type"
  }
  @invalid_attrs %{access_token: nil, refresh_token: nil, social_media: nil, type: nil}

  def fixture(:social) do
    {:ok, social} = Credential.create_social(@create_attrs)
    social
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all socials", %{conn: conn} do
      conn = get(conn, Routes.social_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create social" do
    test "renders social when data is valid", %{conn: conn} do
      conn = post(conn, Routes.social_path(conn, :create), social: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.social_path(conn, :show, id))

      assert %{
               "id" => id,
               "access_token" => "some access_token",
               "refresh_token" => "some refresh_token",
               "social_media" => "some social_media",
               "type" => "some type"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.social_path(conn, :create), social: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update social" do
    setup [:create_social]

    test "renders social when data is valid", %{conn: conn, social: %Social{id: id} = social} do
      conn = put(conn, Routes.social_path(conn, :update, social), social: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.social_path(conn, :show, id))

      assert %{
               "id" => id,
               "access_token" => "some updated access_token",
               "refresh_token" => "some updated refresh_token",
               "social_media" => "some updated social_media",
               "type" => "some updated type"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, social: social} do
      conn = put(conn, Routes.social_path(conn, :update, social), social: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete social" do
    setup [:create_social]

    test "deletes chosen social", %{conn: conn, social: social} do
      conn = delete(conn, Routes.social_path(conn, :delete, social))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.social_path(conn, :show, social))
      end
    end
  end

  defp create_social(_) do
    social = fixture(:social)
    %{social: social}
  end
end

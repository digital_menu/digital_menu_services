defmodule DigitalMenuServices.RestaurantTest do
  use DigitalMenuServices.DataCase

  alias DigitalMenuServices.Restaurant

  describe "menus" do
    alias DigitalMenuServices.Restaurant.Menu

    @valid_attrs %{name: "some name", schedule: [], type: "some type"}
    @update_attrs %{name: "some updated name", schedule: [], type: "some updated type"}
    @invalid_attrs %{name: nil, schedule: nil, type: nil}

    def menu_fixture(attrs \\ %{}) do
      {:ok, menu} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Restaurant.create_menu()

      menu
    end

    test "list_menus/0 returns all menus" do
      menu = menu_fixture()
      assert Restaurant.list_menus() == [menu]
    end

    test "get_menu!/1 returns the menu with given id" do
      menu = menu_fixture()
      assert Restaurant.get_menu!(menu.id) == menu
    end

    test "create_menu/1 with valid data creates a menu" do
      assert {:ok, %Menu{} = menu} = Restaurant.create_menu(@valid_attrs)
      assert menu.name == "some name"
      assert menu.schedule == []
      assert menu.type == "some type"
    end

    test "create_menu/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Restaurant.create_menu(@invalid_attrs)
    end

    test "update_menu/2 with valid data updates the menu" do
      menu = menu_fixture()
      assert {:ok, %Menu{} = menu} = Restaurant.update_menu(menu, @update_attrs)
      assert menu.name == "some updated name"
      assert menu.schedule == []
      assert menu.type == "some updated type"
    end

    test "update_menu/2 with invalid data returns error changeset" do
      menu = menu_fixture()
      assert {:error, %Ecto.Changeset{}} = Restaurant.update_menu(menu, @invalid_attrs)
      assert menu == Restaurant.get_menu!(menu.id)
    end

    test "delete_menu/1 deletes the menu" do
      menu = menu_fixture()
      assert {:ok, %Menu{}} = Restaurant.delete_menu(menu)
      assert_raise Ecto.NoResultsError, fn -> Restaurant.get_menu!(menu.id) end
    end

    test "change_menu/1 returns a menu changeset" do
      menu = menu_fixture()
      assert %Ecto.Changeset{} = Restaurant.change_menu(menu)
    end
  end

  describe "settings" do
    alias DigitalMenuServices.Restaurant.Setting

    @valid_attrs %{font_type: "some font_type", main_color: "some main_color", promo_timer: 42, secondary_color: "some secondary_color", text_color: "some text_color"}
    @update_attrs %{font_type: "some updated font_type", main_color: "some updated main_color", promo_timer: 43, secondary_color: "some updated secondary_color", text_color: "some updated text_color"}
    @invalid_attrs %{font_type: nil, main_color: nil, promo_timer: nil, secondary_color: nil, text_color: nil}

    def setting_fixture(attrs \\ %{}) do
      {:ok, setting} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Restaurant.create_setting()

      setting
    end

    test "list_settings/0 returns all settings" do
      setting = setting_fixture()
      assert Restaurant.list_settings() == [setting]
    end

    test "get_setting!/1 returns the setting with given id" do
      setting = setting_fixture()
      assert Restaurant.get_setting!(setting.id) == setting
    end

    test "create_setting/1 with valid data creates a setting" do
      assert {:ok, %Setting{} = setting} = Restaurant.create_setting(@valid_attrs)
      assert setting.font_type == "some font_type"
      assert setting.main_color == "some main_color"
      assert setting.promo_timer == 42
      assert setting.secondary_color == "some secondary_color"
      assert setting.text_color == "some text_color"
    end

    test "create_setting/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Restaurant.create_setting(@invalid_attrs)
    end

    test "update_setting/2 with valid data updates the setting" do
      setting = setting_fixture()
      assert {:ok, %Setting{} = setting} = Restaurant.update_setting(setting, @update_attrs)
      assert setting.font_type == "some updated font_type"
      assert setting.main_color == "some updated main_color"
      assert setting.promo_timer == 43
      assert setting.secondary_color == "some updated secondary_color"
      assert setting.text_color == "some updated text_color"
    end

    test "update_setting/2 with invalid data returns error changeset" do
      setting = setting_fixture()
      assert {:error, %Ecto.Changeset{}} = Restaurant.update_setting(setting, @invalid_attrs)
      assert setting == Restaurant.get_setting!(setting.id)
    end

    test "delete_setting/1 deletes the setting" do
      setting = setting_fixture()
      assert {:ok, %Setting{}} = Restaurant.delete_setting(setting)
      assert_raise Ecto.NoResultsError, fn -> Restaurant.get_setting!(setting.id) end
    end

    test "change_setting/1 returns a setting changeset" do
      setting = setting_fixture()
      assert %Ecto.Changeset{} = Restaurant.change_setting(setting)
    end
  end

  describe "promos" do
    alias DigitalMenuServices.Restaurant.Promo

    @valid_attrs %{begin_date: "some begin_date", description: "some description", end_date: "some end_date", image: "some image", name: "some name", type: "some type"}
    @update_attrs %{begin_date: "some updated begin_date", description: "some updated description", end_date: "some updated end_date", image: "some updated image", name: "some updated name", type: "some updated type"}
    @invalid_attrs %{begin_date: nil, description: nil, end_date: nil, image: nil, name: nil, type: nil}

    def promo_fixture(attrs \\ %{}) do
      {:ok, promo} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Restaurant.create_promo()

      promo
    end

    test "list_promos/0 returns all promos" do
      promo = promo_fixture()
      assert Restaurant.list_promos() == [promo]
    end

    test "get_promo!/1 returns the promo with given id" do
      promo = promo_fixture()
      assert Restaurant.get_promo!(promo.id) == promo
    end

    test "create_promo/1 with valid data creates a promo" do
      assert {:ok, %Promo{} = promo} = Restaurant.create_promo(@valid_attrs)
      assert promo.begin_date == "some begin_date"
      assert promo.description == "some description"
      assert promo.end_date == "some end_date"
      assert promo.image == "some image"
      assert promo.name == "some name"
      assert promo.type == "some type"
    end

    test "create_promo/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Restaurant.create_promo(@invalid_attrs)
    end

    test "update_promo/2 with valid data updates the promo" do
      promo = promo_fixture()
      assert {:ok, %Promo{} = promo} = Restaurant.update_promo(promo, @update_attrs)
      assert promo.begin_date == "some updated begin_date"
      assert promo.description == "some updated description"
      assert promo.end_date == "some updated end_date"
      assert promo.image == "some updated image"
      assert promo.name == "some updated name"
      assert promo.type == "some updated type"
    end

    test "update_promo/2 with invalid data returns error changeset" do
      promo = promo_fixture()
      assert {:error, %Ecto.Changeset{}} = Restaurant.update_promo(promo, @invalid_attrs)
      assert promo == Restaurant.get_promo!(promo.id)
    end

    test "delete_promo/1 deletes the promo" do
      promo = promo_fixture()
      assert {:ok, %Promo{}} = Restaurant.delete_promo(promo)
      assert_raise Ecto.NoResultsError, fn -> Restaurant.get_promo!(promo.id) end
    end

    test "change_promo/1 returns a promo changeset" do
      promo = promo_fixture()
      assert %Ecto.Changeset{} = Restaurant.change_promo(promo)
    end
  end
end

defmodule DigitalMenuServices.MenuTest do
  use DigitalMenuServices.DataCase

  alias DigitalMenuServices.Menu

  describe "sections" do
    alias DigitalMenuServices.Menu.Section

    @valid_attrs %{back_image: "some back_image", description: "some description", name: "some name"}
    @update_attrs %{back_image: "some updated back_image", description: "some updated description", name: "some updated name"}
    @invalid_attrs %{back_image: nil, description: nil, name: nil}

    def section_fixture(attrs \\ %{}) do
      {:ok, section} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Menu.create_section()

      section
    end

    test "list_sections/0 returns all sections" do
      section = section_fixture()
      assert Menu.list_sections() == [section]
    end

    test "get_section!/1 returns the section with given id" do
      section = section_fixture()
      assert Menu.get_section!(section.id) == section
    end

    test "create_section/1 with valid data creates a section" do
      assert {:ok, %Section{} = section} = Menu.create_section(@valid_attrs)
      assert section.back_image == "some back_image"
      assert section.description == "some description"
      assert section.name == "some name"
    end

    test "create_section/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Menu.create_section(@invalid_attrs)
    end

    test "update_section/2 with valid data updates the section" do
      section = section_fixture()
      assert {:ok, %Section{} = section} = Menu.update_section(section, @update_attrs)
      assert section.back_image == "some updated back_image"
      assert section.description == "some updated description"
      assert section.name == "some updated name"
    end

    test "update_section/2 with invalid data returns error changeset" do
      section = section_fixture()
      assert {:error, %Ecto.Changeset{}} = Menu.update_section(section, @invalid_attrs)
      assert section == Menu.get_section!(section.id)
    end

    test "delete_section/1 deletes the section" do
      section = section_fixture()
      assert {:ok, %Section{}} = Menu.delete_section(section)
      assert_raise Ecto.NoResultsError, fn -> Menu.get_section!(section.id) end
    end

    test "change_section/1 returns a section changeset" do
      section = section_fixture()
      assert %Ecto.Changeset{} = Menu.change_section(section)
    end
  end

  describe "dishes" do
    alias DigitalMenuServices.Menu.Dishes

    @valid_attrs %{description: "some description", enabled: "some enabled", extras: [], image: "some image", name: "some name", price: "some price"}
    @update_attrs %{description: "some updated description", enabled: "some updated enabled", extras: [], image: "some updated image", name: "some updated name", price: "some updated price"}
    @invalid_attrs %{description: nil, enabled: nil, extras: nil, image: nil, name: nil, price: nil}

    def dishes_fixture(attrs \\ %{}) do
      {:ok, dishes} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Menu.create_dishes()

      dishes
    end

    test "list_dishes/0 returns all dishes" do
      dishes = dishes_fixture()
      assert Menu.list_dishes() == [dishes]
    end

    test "get_dishes!/1 returns the dishes with given id" do
      dishes = dishes_fixture()
      assert Menu.get_dishes!(dishes.id) == dishes
    end

    test "create_dishes/1 with valid data creates a dishes" do
      assert {:ok, %Dishes{} = dishes} = Menu.create_dishes(@valid_attrs)
      assert dishes.description == "some description"
      assert dishes.enabled == "some enabled"
      assert dishes.extras == []
      assert dishes.image == "some image"
      assert dishes.name == "some name"
      assert dishes.price == "some price"
    end

    test "create_dishes/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Menu.create_dishes(@invalid_attrs)
    end

    test "update_dishes/2 with valid data updates the dishes" do
      dishes = dishes_fixture()
      assert {:ok, %Dishes{} = dishes} = Menu.update_dishes(dishes, @update_attrs)
      assert dishes.description == "some updated description"
      assert dishes.enabled == "some updated enabled"
      assert dishes.extras == []
      assert dishes.image == "some updated image"
      assert dishes.name == "some updated name"
      assert dishes.price == "some updated price"
    end

    test "update_dishes/2 with invalid data returns error changeset" do
      dishes = dishes_fixture()
      assert {:error, %Ecto.Changeset{}} = Menu.update_dishes(dishes, @invalid_attrs)
      assert dishes == Menu.get_dishes!(dishes.id)
    end

    test "delete_dishes/1 deletes the dishes" do
      dishes = dishes_fixture()
      assert {:ok, %Dishes{}} = Menu.delete_dishes(dishes)
      assert_raise Ecto.NoResultsError, fn -> Menu.get_dishes!(dishes.id) end
    end

    test "change_dishes/1 returns a dishes changeset" do
      dishes = dishes_fixture()
      assert %Ecto.Changeset{} = Menu.change_dishes(dishes)
    end
  end
end

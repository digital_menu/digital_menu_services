defmodule DigitalMenuServices.AccountTest do
  use DigitalMenuServices.DataCase

  alias DigitalMenuServices.Account

  describe "users" do
    alias DigitalMenuServices.Account.User

    @valid_attrs %{email: "some email", profile_picture: "some profile_picture", username: "some username"}
    @update_attrs %{email: "some updated email", profile_picture: "some updated profile_picture", username: "some updated username"}
    @invalid_attrs %{email: nil, profile_picture: nil, username: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Account.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Account.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Account.create_user(@valid_attrs)
      assert user.email == "some email"
      assert user.profile_picture == "some profile_picture"
      assert user.username == "some username"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Account.update_user(user, @update_attrs)
      assert user.email == "some updated email"
      assert user.profile_picture == "some updated profile_picture"
      assert user.username == "some updated username"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_user(user, @invalid_attrs)
      assert user == Account.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Account.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Account.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Account.change_user(user)
    end
  end

  describe "restaurants" do
    alias DigitalMenuServices.Account.Restaurant

    @valid_attrs %{address: "some address", coordinates: "some coordinates", description: "some description", enabled: "some enabled", image: "some image", name: "some name"}
    @update_attrs %{address: "some updated address", coordinates: "some updated coordinates", description: "some updated description", enabled: "some updated enabled", image: "some updated image", name: "some updated name"}
    @invalid_attrs %{address: nil, coordinates: nil, description: nil, enabled: nil, image: nil, name: nil}

    def restaurant_fixture(attrs \\ %{}) do
      {:ok, restaurant} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_restaurant()

      restaurant
    end

    test "list_restaurants/0 returns all restaurants" do
      restaurant = restaurant_fixture()
      assert Account.list_restaurants() == [restaurant]
    end

    test "get_restaurant!/1 returns the restaurant with given id" do
      restaurant = restaurant_fixture()
      assert Account.get_restaurant!(restaurant.id) == restaurant
    end

    test "create_restaurant/1 with valid data creates a restaurant" do
      assert {:ok, %Restaurant{} = restaurant} = Account.create_restaurant(@valid_attrs)
      assert restaurant.address == "some address"
      assert restaurant.coordinates == "some coordinates"
      assert restaurant.description == "some description"
      assert restaurant.enabled == "some enabled"
      assert restaurant.image == "some image"
      assert restaurant.name == "some name"
    end

    test "create_restaurant/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_restaurant(@invalid_attrs)
    end

    test "update_restaurant/2 with valid data updates the restaurant" do
      restaurant = restaurant_fixture()
      assert {:ok, %Restaurant{} = restaurant} = Account.update_restaurant(restaurant, @update_attrs)
      assert restaurant.address == "some updated address"
      assert restaurant.coordinates == "some updated coordinates"
      assert restaurant.description == "some updated description"
      assert restaurant.enabled == "some updated enabled"
      assert restaurant.image == "some updated image"
      assert restaurant.name == "some updated name"
    end

    test "update_restaurant/2 with invalid data returns error changeset" do
      restaurant = restaurant_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_restaurant(restaurant, @invalid_attrs)
      assert restaurant == Account.get_restaurant!(restaurant.id)
    end

    test "delete_restaurant/1 deletes the restaurant" do
      restaurant = restaurant_fixture()
      assert {:ok, %Restaurant{}} = Account.delete_restaurant(restaurant)
      assert_raise Ecto.NoResultsError, fn -> Account.get_restaurant!(restaurant.id) end
    end

    test "change_restaurant/1 returns a restaurant changeset" do
      restaurant = restaurant_fixture()
      assert %Ecto.Changeset{} = Account.change_restaurant(restaurant)
    end
  end
end

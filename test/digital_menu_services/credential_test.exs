defmodule DigitalMenuServices.CredentialTest do
  use DigitalMenuServices.DataCase

  alias DigitalMenuServices.Credential

  describe "internals" do
    alias DigitalMenuServices.Credential.Internal

    @valid_attrs %{password: "some password", type: "some type"}
    @update_attrs %{password: "some updated password", type: "some updated type"}
    @invalid_attrs %{password: nil, type: nil}

    def internal_fixture(attrs \\ %{}) do
      {:ok, internal} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Credential.create_internal()

      internal
    end

    test "list_internals/0 returns all internals" do
      internal = internal_fixture()
      assert Credential.list_internals() == [internal]
    end

    test "get_internal!/1 returns the internal with given id" do
      internal = internal_fixture()
      assert Credential.get_internal!(internal.id) == internal
    end

    test "create_internal/1 with valid data creates a internal" do
      assert {:ok, %Internal{} = internal} = Credential.create_internal(@valid_attrs)
      assert internal.password == "some password"
      assert internal.type == "some type"
    end

    test "create_internal/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Credential.create_internal(@invalid_attrs)
    end

    test "update_internal/2 with valid data updates the internal" do
      internal = internal_fixture()
      assert {:ok, %Internal{} = internal} = Credential.update_internal(internal, @update_attrs)
      assert internal.password == "some updated password"
      assert internal.type == "some updated type"
    end

    test "update_internal/2 with invalid data returns error changeset" do
      internal = internal_fixture()
      assert {:error, %Ecto.Changeset{}} = Credential.update_internal(internal, @invalid_attrs)
      assert internal == Credential.get_internal!(internal.id)
    end

    test "delete_internal/1 deletes the internal" do
      internal = internal_fixture()
      assert {:ok, %Internal{}} = Credential.delete_internal(internal)
      assert_raise Ecto.NoResultsError, fn -> Credential.get_internal!(internal.id) end
    end

    test "change_internal/1 returns a internal changeset" do
      internal = internal_fixture()
      assert %Ecto.Changeset{} = Credential.change_internal(internal)
    end
  end

  describe "socials" do
    alias DigitalMenuServices.Credential.Social

    @valid_attrs %{access_token: "some access_token", refresh_token: "some refresh_token", social_media: "some social_media", type: "some type"}
    @update_attrs %{access_token: "some updated access_token", refresh_token: "some updated refresh_token", social_media: "some updated social_media", type: "some updated type"}
    @invalid_attrs %{access_token: nil, refresh_token: nil, social_media: nil, type: nil}

    def social_fixture(attrs \\ %{}) do
      {:ok, social} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Credential.create_social()

      social
    end

    test "list_socials/0 returns all socials" do
      social = social_fixture()
      assert Credential.list_socials() == [social]
    end

    test "get_social!/1 returns the social with given id" do
      social = social_fixture()
      assert Credential.get_social!(social.id) == social
    end

    test "create_social/1 with valid data creates a social" do
      assert {:ok, %Social{} = social} = Credential.create_social(@valid_attrs)
      assert social.access_token == "some access_token"
      assert social.refresh_token == "some refresh_token"
      assert social.social_media == "some social_media"
      assert social.type == "some type"
    end

    test "create_social/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Credential.create_social(@invalid_attrs)
    end

    test "update_social/2 with valid data updates the social" do
      social = social_fixture()
      assert {:ok, %Social{} = social} = Credential.update_social(social, @update_attrs)
      assert social.access_token == "some updated access_token"
      assert social.refresh_token == "some updated refresh_token"
      assert social.social_media == "some updated social_media"
      assert social.type == "some updated type"
    end

    test "update_social/2 with invalid data returns error changeset" do
      social = social_fixture()
      assert {:error, %Ecto.Changeset{}} = Credential.update_social(social, @invalid_attrs)
      assert social == Credential.get_social!(social.id)
    end

    test "delete_social/1 deletes the social" do
      social = social_fixture()
      assert {:ok, %Social{}} = Credential.delete_social(social)
      assert_raise Ecto.NoResultsError, fn -> Credential.get_social!(social.id) end
    end

    test "change_social/1 returns a social changeset" do
      social = social_fixture()
      assert %Ecto.Changeset{} = Credential.change_social(social)
    end
  end
end

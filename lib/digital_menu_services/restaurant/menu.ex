defmodule DigitalMenuServices.Restaurant.Menu do
  use Ecto.Schema
  import Ecto.Changeset

  alias DigitalMenuServices.Account.{Restaurant, User}
  alias DigitalMenuServices.Menu.Section

  schema "menus" do
    field :name, :string
    field :schedule, {:array, :string}
    field :type, :string
    field :image, :string

    belongs_to :user, User
    belongs_to :restaurant, Restaurant

    has_many :sections, Section

    timestamps()
  end

  @doc false
  def changeset(menu, attrs) do
    menu
    |> cast(attrs, [:name, :type, :schedule, :image, :user_id, :restaurant_id])
    |> validate_required([:name, :type, :user_id, :restaurant_id])
  end
end

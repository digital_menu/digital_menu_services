defmodule DigitalMenuServices.Restaurant.Promo do
  use Ecto.Schema
  import Ecto.Changeset

  alias DigitalMenuServices.Account.{Restaurant, User}

  schema "promos" do
    field :begin_date, :string
    field :description, :string
    field :end_date, :string
    field :image, :string
    field :name, :string
    field :type, :string

    belongs_to :user, User
    belongs_to :restaurant, Restaurant

    timestamps()
  end

  @doc false
  def changeset(promo, attrs) do
    promo
    |> cast(attrs, [:name, :description, :type, :image, :begin_date, :end_date, :user_id, :restaurant_id])
    |> validate_required([:name, :description, :type, :image, :begin_date, :end_date, :user_id, :restaurant_id])
  end
end

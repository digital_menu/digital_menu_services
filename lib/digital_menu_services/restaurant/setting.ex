defmodule DigitalMenuServices.Restaurant.Setting do
  use Ecto.Schema
  import Ecto.Changeset

  alias DigitalMenuServices.Account.{Restaurant, User}

  schema "settings" do
    field :font_type, :string
    field :main_color, :string
    field :promo_timer, :integer
    field :secondary_color, :string
    field :text_color, :string
    field :secondary_text_color, :string

    belongs_to :user, User
    belongs_to :restaurant, Restaurant

    timestamps()
  end

  @doc false
  def changeset(setting, attrs) do
    setting
    |> cast(attrs, [:main_color, :secondary_color, :text_color, :secondary_text_color, :promo_timer, :font_type, :user_id, :restaurant_id])
    |> validate_required([:main_color, :secondary_color, :text_color, :secondary_text_color, :user_id, :restaurant_id])
  end
end

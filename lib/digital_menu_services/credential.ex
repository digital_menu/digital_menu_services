defmodule DigitalMenuServices.Credential do
  @moduledoc """
  The Credential context.
  """

  import Ecto.Query, warn: false
  alias DigitalMenuServices.Repo

  alias DigitalMenuServices.Credential.Internal

  @doc """
  Returns the list of internals.

  ## Examples

      iex> list_internals()
      [%Internal{}, ...]

  """
  def list_internals do
    Repo.all(Internal)
  end

  @doc """
  Gets a single internal.

  Raises `Ecto.NoResultsError` if the Internal does not exist.

  ## Examples

      iex> get_internal!(123)
      %Internal{}

      iex> get_internal!(456)
      ** (Ecto.NoResultsError)

  """
  def get_internal!(id), do: Repo.get!(Internal, id)

  @doc """
  Creates a internal.

  ## Examples

      iex> create_internal(%{field: value})
      {:ok, %Internal{}}

      iex> create_internal(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_internal(attrs \\ %{}) do
    %Internal{}
    |> Internal.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a internal.

  ## Examples

      iex> update_internal(internal, %{field: new_value})
      {:ok, %Internal{}}

      iex> update_internal(internal, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_internal(%Internal{} = internal, attrs) do
    internal
    |> Internal.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a internal.

  ## Examples

      iex> delete_internal(internal)
      {:ok, %Internal{}}

      iex> delete_internal(internal)
      {:error, %Ecto.Changeset{}}

  """
  def delete_internal(%Internal{} = internal) do
    Repo.delete(internal)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking internal changes.

  ## Examples

      iex> change_internal(internal)
      %Ecto.Changeset{data: %Internal{}}

  """
  def change_internal(%Internal{} = internal, attrs \\ %{}) do
    Internal.changeset(internal, attrs)
  end

  alias DigitalMenuServices.Credential.Social

  @doc """
  Returns the list of socials.

  ## Examples

      iex> list_socials()
      [%Social{}, ...]

  """
  def list_socials do
    Repo.all(Social)
  end

  @doc """
  Gets a single social.

  Raises `Ecto.NoResultsError` if the Social does not exist.

  ## Examples

      iex> get_social!(123)
      %Social{}

      iex> get_social!(456)
      ** (Ecto.NoResultsError)

  """
  def get_social!(id), do: Repo.get!(Social, id)

  @doc """
  Creates a social.

  ## Examples

      iex> create_social(%{field: value})
      {:ok, %Social{}}

      iex> create_social(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_social(attrs \\ %{}) do
    %Social{}
    |> Social.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a social.

  ## Examples

      iex> update_social(social, %{field: new_value})
      {:ok, %Social{}}

      iex> update_social(social, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_social(%Social{} = social, attrs) do
    social
    |> Social.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a social.

  ## Examples

      iex> delete_social(social)
      {:ok, %Social{}}

      iex> delete_social(social)
      {:error, %Ecto.Changeset{}}

  """
  def delete_social(%Social{} = social) do
    Repo.delete(social)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking social changes.

  ## Examples

      iex> change_social(social)
      %Ecto.Changeset{data: %Social{}}

  """
  def change_social(%Social{} = social, attrs \\ %{}) do
    Social.changeset(social, attrs)
  end
end

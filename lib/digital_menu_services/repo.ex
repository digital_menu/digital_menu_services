defmodule DigitalMenuServices.Repo do
  use Ecto.Repo,
    otp_app: :digital_menu_services,
    adapter: Ecto.Adapters.Postgres
end

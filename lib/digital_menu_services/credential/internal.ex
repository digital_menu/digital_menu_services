defmodule DigitalMenuServices.Credential.Internal do
  use Ecto.Schema
  import Ecto.Changeset

  alias DigitalMenuServices.Account.User

  schema "internals" do
    field :password, :string
    field :type, :string

    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(internal, attrs) do
    internal
    |> cast(attrs, [:password, :type])
    |> validate_required([:password, :type])
    |> encrypt_pass()
  end

  defp encrypt_pass(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password, Bcrypt.hash_pwd_salt(pass))
      _ -> changeset
    end
  end
end

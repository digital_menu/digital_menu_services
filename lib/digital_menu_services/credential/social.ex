defmodule DigitalMenuServices.Credential.Social do
  use Ecto.Schema
  import Ecto.Changeset

  schema "socials" do
    field :access_token, :string
    field :refresh_token, :string
    field :social_media, :string
    field :type, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(social, attrs) do
    social
    |> cast(attrs, [:access_token, :refresh_token, :type, :social_media])
    |> validate_required([:access_token, :refresh_token, :type, :social_media])
  end
end

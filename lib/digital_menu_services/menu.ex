defmodule DigitalMenuServices.Menu do
  @moduledoc """
  The Menu context.
  """

  import Ecto.Query, warn: false
  alias DigitalMenuServices.Repo

  alias DigitalMenuServices.Menu.Section

  @doc """
  Returns the list of sections.

  ## Examples

      iex> list_sections()
      [%Section{}, ...]

  """
  def list_sections(user_id, restaurant_id, menu_id) do
    Section
    |> where([section], section.user_id == ^user_id and section.restaurant_id == ^restaurant_id and section.menu_id == ^menu_id)
    |> Repo.all()
  end

  @doc """
  Gets a single section.

  Raises `Ecto.NoResultsError` if the Section does not exist.

  ## Examples

      iex> get_section!(123)
      %Section{}

      iex> get_section!(456)
      ** (Ecto.NoResultsError)

  """
  def get_section!(id, user_id, restaurant_id, menu_id) do
    Section
    |> where([section], section.user_id == ^user_id and section.restaurant_id == ^restaurant_id and section.menu_id == ^menu_id)
    |> Repo.get!(id)
  end

  @doc """
  Creates a section.

  ## Examples

      iex> create_section(%{field: value})
      {:ok, %Section{}}

      iex> create_section(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_section(attrs \\ %{}) do
    %Section{}
    |> Section.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a section.

  ## Examples

      iex> update_section(section, %{field: new_value})
      {:ok, %Section{}}

      iex> update_section(section, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_section(%Section{} = section, attrs) do
    section
    |> Section.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a section.

  ## Examples

      iex> delete_section(section)
      {:ok, %Section{}}

      iex> delete_section(section)
      {:error, %Ecto.Changeset{}}

  """
  def delete_section(%Section{} = section) do
    Repo.delete(section)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking section changes.

  ## Examples

      iex> change_section(section)
      %Ecto.Changeset{data: %Section{}}

  """
  def change_section(%Section{} = section, attrs \\ %{}) do
    Section.changeset(section, attrs)
  end

  alias DigitalMenuServices.Menu.Dishes

  @doc """
  Returns the list of dishes.

  ## Examples

      iex> list_dishes()
      [%Dishes{}, ...]

  """
  def list_dishes(user_id, restaurant_id, menu_id, section_id) do
    Dishes
    |> where([dish], dish.user_id == ^user_id and dish.restaurant_id == ^restaurant_id and dish.menu_id == ^menu_id and dish.section_id == ^section_id)
    |> Repo.all()
  end

  @doc """
  Gets a single dishes.

  Raises `Ecto.NoResultsError` if the Dishes does not exist.

  ## Examples

      iex> get_dishes!(123)
      %Dishes{}

      iex> get_dishes!(456)
      ** (Ecto.NoResultsError)

  """
  def get_dishes!(id, user_id, restaurant_id, menu_id, section_id) do
    Dishes
    |> where([dish], dish.user_id == ^user_id and dish.restaurant_id == ^restaurant_id and dish.menu_id == ^menu_id and dish.section_id == ^section_id)
    |> Repo.get!(id)
  end

  @doc """
  Creates a dishes.

  ## Examples

      iex> create_dishes(%{field: value})
      {:ok, %Dishes{}}

      iex> create_dishes(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_dishes(attrs \\ %{}) do
    %Dishes{}
    |> Dishes.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a dishes.

  ## Examples

      iex> update_dishes(dishes, %{field: new_value})
      {:ok, %Dishes{}}

      iex> update_dishes(dishes, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_dishes(%Dishes{} = dishes, attrs) do
    dishes
    |> Dishes.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a dishes.

  ## Examples

      iex> delete_dishes(dishes)
      {:ok, %Dishes{}}

      iex> delete_dishes(dishes)
      {:error, %Ecto.Changeset{}}

  """
  def delete_dishes(%Dishes{} = dishes) do
    Repo.delete(dishes)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking dishes changes.

  ## Examples

      iex> change_dishes(dishes)
      %Ecto.Changeset{data: %Dishes{}}

  """
  def change_dishes(%Dishes{} = dishes, attrs \\ %{}) do
    Dishes.changeset(dishes, attrs)
  end
end

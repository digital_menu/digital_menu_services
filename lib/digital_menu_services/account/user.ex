defmodule DigitalMenuServices.Account.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias DigitalMenuServices.Credential.{ Internal, Social }
  alias DigitalMenuServices.Account.Restaurant

  schema "users" do
    field :email, :string
    field :profile_picture, :string
    field :username, :string

    has_one :internal, Internal
    has_many :restaurants, Restaurant

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :profile_picture])
    |> validate_required([:username, :email])
    |> unique_constraint(:email)
  end
end

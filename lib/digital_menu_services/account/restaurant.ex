defmodule DigitalMenuServices.Account.Restaurant do
  use Ecto.Schema
  import Ecto.Changeset

  alias DigitalMenuServices.Account.User
  alias DigitalMenuServices.Restaurant.{Menu,Setting,Promo}
  schema "restaurants" do
    field :address, :string
    field :coordinates, :string
    field :description, :string
    field :enabled, :boolean, default: false
    field :image, :string
    field :name, :string

    has_many :menus, Menu
    has_many :promos, Promo
    has_one :setting, Setting

    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(restaurant, attrs) do
    restaurant
    |> cast(attrs, [:name, :image, :description, :address, :coordinates, :enabled, :user_id])
    |> validate_required([:name, :description, :address, :user_id])
    |> unique_constraint([:name])
  end
end

defmodule DigitalMenuServices.Menu.Section do
  use Ecto.Schema
  import Ecto.Changeset

  alias DigitalMenuServices.Account.{Restaurant, User}
  alias DigitalMenuServices.Restaurant.Menu
  alias DigitalMenuServices.Menu.Dishes

  schema "sections" do
    field :back_image, :string
    field :description, :string
    field :name, :string

    belongs_to :user, User
    belongs_to :restaurant, Restaurant
    belongs_to :menu, Menu

    has_many :dishes, Dishes
    timestamps()
  end

  @doc false
  def changeset(section, attrs) do
    section
    |> cast(attrs, [:name, :description, :back_image, :user_id, :restaurant_id, :menu_id])
    |> validate_required([:name, :description, :user_id, :restaurant_id, :menu_id])
  end
end

defmodule DigitalMenuServices.Menu.Dishes do
  use Ecto.Schema
  import Ecto.Changeset

  alias DigitalMenuServices.Account.{Restaurant, User}
  alias DigitalMenuServices.Restaurant.Menu
  alias DigitalMenuServices.Menu.Section

  schema "dishes" do
    field :description, :string
    field :enabled, :boolean, default: false
    field :extras, {:array, :string}
    field :image, :string
    field :name, :string
    field :price, :string

    belongs_to :user, User
    belongs_to :restaurant, Restaurant
    belongs_to :menu, Menu
    belongs_to :section, Section

    timestamps()
  end

  @doc false
  def changeset(dishes, attrs) do
    dishes
    |> cast(attrs, [:name, :description, :image, :price, :enabled, :extras, :user_id, :restaurant_id, :menu_id, :section_id])
    |> validate_required([:name, :description,  :price, :enabled, :user_id, :restaurant_id, :menu_id, :section_id])
  end
end

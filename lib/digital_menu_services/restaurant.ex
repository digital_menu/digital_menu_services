defmodule DigitalMenuServices.Restaurant do
  @moduledoc """
  The Restaurant context.
  """

  import Ecto.Query, warn: false
  alias DigitalMenuServices.Repo

  alias DigitalMenuServices.Restaurant.Menu

  @doc """
  Returns the list of menus.

  ## Examples

      iex> list_menus()
      [%Menu{}, ...]

  """
  def list_menus(user_id, restaurant_id) do
    Menu
    |> where([menu], menu.user_id == ^user_id and menu.restaurant_id == ^restaurant_id)
    |> Repo.all()
  end

  @doc """
  Gets a single menu.

  Raises `Ecto.NoResultsError` if the Menu does not exist.

  ## Examples

      iex> get_menu!(123)
      %Menu{}

      iex> get_menu!(456)
      ** (Ecto.NoResultsError)

  """
  def get_menu_by_name!(menu, user_id, restaurant_id) do
    Menu
    |> where([menu], menu.user_id == ^user_id and menu.restaurant_id == ^restaurant_id)
    |> Repo.get_by!(name: menu)
    |> Repo.preload(sections: [:dishes])
  end

  def get_menu!(id, user_id, restaurant_id) do
    Menu
    |> where([menu], menu.user_id == ^user_id and menu.restaurant_id == ^restaurant_id)
    |> Repo.get!(id)
  end

  @doc """
  Creates a menu.

  ## Examples

      iex> create_menu(%{field: value})
      {:ok, %Menu{}}

      iex> create_menu(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_menu(attrs \\ %{}) do
    %Menu{}
    |> Menu.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a menu.

  ## Examples

      iex> update_menu(menu, %{field: new_value})
      {:ok, %Menu{}}

      iex> update_menu(menu, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_menu(%Menu{} = menu, attrs) do
    menu
    |> Menu.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a menu.

  ## Examples

      iex> delete_menu(menu)
      {:ok, %Menu{}}

      iex> delete_menu(menu)
      {:error, %Ecto.Changeset{}}

  """
  def delete_menu(%Menu{} = menu) do
    Repo.delete(menu)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking menu changes.

  ## Examples

      iex> change_menu(menu)
      %Ecto.Changeset{data: %Menu{}}

  """
  def change_menu(%Menu{} = menu, attrs \\ %{}) do
    Menu.changeset(menu, attrs)
  end

  alias DigitalMenuServices.Restaurant.Setting

  @doc """
  Returns the list of settings.

  ## Examples

      iex> list_settings()
      [%Setting{}, ...]

  """
  def list_settings(user_id, restaurant_id) do
    Setting
    |> where([setting], setting.user_id == ^user_id and setting.restaurant_id == ^restaurant_id)
    |> Repo.all()
  end

  @doc """
  Gets a single setting.

  Raises `Ecto.NoResultsError` if the Setting does not exist.

  ## Examples

      iex> get_setting!(123)
      %Setting{}

      iex> get_setting!(456)
      ** (Ecto.NoResultsError)

  """
  def get_setting!(id, user_id, restaurant_id) do
    Setting
    |> where([setting], setting.user_id == ^user_id and setting.restaurant_id == ^restaurant_id)
    |> Repo.get!(id)
  end

  @doc """
  Creates a setting.

  ## Examples

      iex> create_setting(%{field: value})
      {:ok, %Setting{}}

      iex> create_setting(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_setting(attrs \\ %{}) do
    %Setting{}
    |> Setting.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a setting.

  ## Examples

      iex> update_setting(setting, %{field: new_value})
      {:ok, %Setting{}}

      iex> update_setting(setting, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_setting(%Setting{} = setting, attrs) do
    setting
    |> Setting.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a setting.

  ## Examples

      iex> delete_setting(setting)
      {:ok, %Setting{}}

      iex> delete_setting(setting)
      {:error, %Ecto.Changeset{}}

  """
  def delete_setting(%Setting{} = setting) do
    Repo.delete(setting)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking setting changes.

  ## Examples

      iex> change_setting(setting)
      %Ecto.Changeset{data: %Setting{}}

  """
  def change_setting(%Setting{} = setting, attrs \\ %{}) do
    Setting.changeset(setting, attrs)
  end

  alias DigitalMenuServices.Restaurant.Promo

  @doc """
  Returns the list of promos.

  ## Examples

      iex> list_promos()
      [%Promo{}, ...]

  """
  def list_promos(user_id, restaurant_id) do
    Promo
    |> where([promo], promo.user_id == ^user_id and promo.restaurant_id == ^restaurant_id)
    |> Repo.all()
  end

  @doc """
  Gets a single promo.

  Raises `Ecto.NoResultsError` if the Promo does not exist.

  ## Examples

      iex> get_promo!(123)
      %Promo{}

      iex> get_promo!(456)
      ** (Ecto.NoResultsError)

  """
  def get_promo!(id, user_id, restaurant_id) do
    Promo
    |> where([promo], promo.user_id == ^user_id and promo.restaurant_id == ^restaurant_id)
    |> Repo.get!(id)

  end

  @doc """
  Creates a promo.

  ## Examples

      iex> create_promo(%{field: value})
      {:ok, %Promo{}}

      iex> create_promo(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_promo(attrs \\ %{}) do
    %Promo{}
    |> Promo.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a promo.

  ## Examples

      iex> update_promo(promo, %{field: new_value})
      {:ok, %Promo{}}

      iex> update_promo(promo, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_promo(%Promo{} = promo, attrs) do
    promo
    |> Promo.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a promo.

  ## Examples

      iex> delete_promo(promo)
      {:ok, %Promo{}}

      iex> delete_promo(promo)
      {:error, %Ecto.Changeset{}}

  """
  def delete_promo(%Promo{} = promo) do
    Repo.delete(promo)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking promo changes.

  ## Examples

      iex> change_promo(promo)
      %Ecto.Changeset{data: %Promo{}}

  """
  def change_promo(%Promo{} = promo, attrs \\ %{}) do
    Promo.changeset(promo, attrs)
  end
end

defmodule DigitalMenuServicesWeb.RestaurantController do
  use DigitalMenuServicesWeb, :controller

  alias DigitalMenuServices.Account
  alias DigitalMenuServices.Account.Restaurant

  action_fallback DigitalMenuServicesWeb.FallbackController

  def get_restaurant(conn, %{"name" => name}) do
    restaurant = Account.get_restaurant_by_name!(name)
    render(conn, "public.json", restaurant: restaurant)
  end

  def index(conn, %{"user_id" => user_id}) do
    restaurants = Account.list_restaurants(user_id)
    render(conn, "index.json", restaurants: restaurants)
  end

  def create(conn, %{"restaurant" => restaurant_params, "user_id" => user_id}) do
    restaurant_params = restaurant_params
    |> Map.put("user_id", user_id)
    res = if Map.has_key?(restaurant_params, "image") and restaurant_params["image"] != "" do
      {start, length} = :binary.match(restaurant_params["image"], ";base64,")
      raw = :binary.part(restaurant_params["image"], start + length, byte_size(restaurant_params["image"]) - start - length)
      status = ExAws.S3.put_object("digital-menu-dev", "restaurants/"<>restaurant_params["name"]<>".png", Base.decode64!(raw))
      |> ExAws.request!()
      |> get_in([:status_code])
      case status do
        200 -> Map.put(restaurant_params, "image", "https://digital-menu-dev.s3-us-west-2.amazonaws.com/restaurants/"<>restaurant_params["name"]<>".png")
        _ -> Map.put(restaurant_params, "image", nil)
      end
    end

    if res do
      with {:ok, %Restaurant{} = restaurant} <- Account.create_restaurant(res) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_restaurant_path(conn, :show, user_id, restaurant))
        |> render("show.json", restaurant: restaurant)
      end
    else
      with {:ok, %Restaurant{} = restaurant} <- Account.create_restaurant(restaurant_params) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_restaurant_path(conn, :show, user_id, restaurant))
        |> render("show.json", restaurant: restaurant)
      end
    end
  end

  def show(conn, %{"id" => id, "user_id" => user_id}) do
    restaurant = Account.get_restaurant!(id, user_id)
    render(conn, "show.json", restaurant: restaurant)
  end

  def update(conn, %{"id" => id, "restaurant" => restaurant_params, "user_id" => user_id}) do
    restaurant = Account.get_restaurant!(id, user_id)

    with {:ok, %Restaurant{} = restaurant} <- Account.update_restaurant(restaurant, restaurant_params) do
      render(conn, "show.json", restaurant: restaurant)
    end
  end

  def delete(conn, %{"id" => id, "user_id" => user_id}) do
    restaurant = Account.get_restaurant!(id, user_id)

    with {:ok, %Restaurant{}} <- Account.delete_restaurant(restaurant) do
      send_resp(conn, :no_content, "")
    end
  end
end

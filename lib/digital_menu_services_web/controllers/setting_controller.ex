defmodule DigitalMenuServicesWeb.SettingController do
  use DigitalMenuServicesWeb, :controller

  alias DigitalMenuServices.Restaurant
  alias DigitalMenuServices.Restaurant.Setting

  action_fallback DigitalMenuServicesWeb.FallbackController

  def index(conn, %{"user_id" => user_id, "restaurant_id" => restaurant_id}) do
    settings = Restaurant.list_settings(user_id, restaurant_id)
    render(conn, "index.json", settings: settings)
  end

  def create(conn, %{"setting" => setting_params, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    setting_params = setting_params
    |> Map.put("user_id", user_id)
    |> Map.put("restaurant_id", restaurant_id)

    with {:ok, %Setting{} = setting} <- Restaurant.create_setting(setting_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_restaurant_setting_path(conn, :show, user_id, restaurant_id, setting))
      |> render("show.json", setting: setting)
    end
  end

  def show(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    setting = Restaurant.get_setting!(id, user_id, restaurant_id)
    render(conn, "show.json", setting: setting)
  end

  def update(conn, %{"id" => id, "setting" => setting_params, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    setting = Restaurant.get_setting!(id, user_id, restaurant_id)

    with {:ok, %Setting{} = setting} <- Restaurant.update_setting(setting, setting_params) do
      render(conn, "show.json", setting: setting)
    end
  end

  def delete(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    setting = Restaurant.get_setting!(id, user_id, restaurant_id)

    with {:ok, %Setting{}} <- Restaurant.delete_setting(setting) do
      send_resp(conn, :no_content, "")
    end
  end
end

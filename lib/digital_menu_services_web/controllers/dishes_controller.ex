defmodule DigitalMenuServicesWeb.DishesController do
  use DigitalMenuServicesWeb, :controller

  alias DigitalMenuServices.Menu
  alias DigitalMenuServices.Menu.Dishes

  action_fallback DigitalMenuServicesWeb.FallbackController

  def index(conn, %{"user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id, "section_id" => section_id}) do
    dishes = Menu.list_dishes(user_id, restaurant_id, menu_id, section_id)
    render(conn, "index.json", dishes: dishes)
  end

  def create(conn, %{"dishes" => dishes_params, "user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id, "section_id" => section_id}) do
    dishes_params = dishes_params
    |> Map.put("user_id", user_id)
    |> Map.put("restaurant_id", restaurant_id)
    |> Map.put("menu_id", menu_id)
    |> Map.put("section_id", section_id)

    res = if Map.has_key?(dishes_params, "image") and dishes_params["image"] do
      {start, length} = :binary.match(dishes_params["image"], ";base64,")
      raw = :binary.part(dishes_params["image"], start + length, byte_size(dishes_params["image"]) - start - length)
      status = ExAws.S3.put_object("digital-menu-dev", "dishes/"<>user_id<>restaurant_id<>menu_id<>section_id<>dishes_params["name"]<>".png", Base.decode64!(raw))
      |> ExAws.request!()
      |> get_in([:status_code])
      case status do
        200 -> Map.put(dishes_params, "image", "https://digital-menu-dev.s3-us-west-2.amazonaws.com/dishes/"<>user_id<>restaurant_id<>menu_id<>section_id<>dishes_params["name"]<>".png")
        _ -> Map.put(dishes_params, "image", nil)
      end
    end

    if res do
      with {:ok, %Dishes{} = dishes} <- Menu.create_dishes(res) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_restaurant_menu_section_dishes_path(conn, :show,  user_id, restaurant_id, menu_id, section_id, dishes))
        |> render("show.json", dishes: dishes)
      end
    else
      with {:ok, %Dishes{} = dishes} <- Menu.create_dishes(dishes_params) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_restaurant_menu_section_dishes_path(conn, :show,  user_id, restaurant_id, menu_id, section_id, dishes))
        |> render("show.json", dishes: dishes)
      end
    end
  end

  def show(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id, "section_id" => section_id}) do
    dishes = Menu.get_dishes!(id, user_id, restaurant_id, menu_id, section_id)
    render(conn, "show.json", dishes: dishes)
  end

  def update(conn, %{"id" => id, "dishes" => dishes_params, "user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id, "section_id" => section_id}) do
    dishes = Menu.get_dishes!(id, user_id, restaurant_id, menu_id, section_id)

    with {:ok, %Dishes{} = dishes} <- Menu.update_dishes(dishes, dishes_params) do
      render(conn, "show.json", dishes: dishes)
    end
  end

  def delete(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id, "section_id" => section_id}) do
    dishes = Menu.get_dishes!(id, user_id, restaurant_id, menu_id, section_id)

    with {:ok, %Dishes{}} <- Menu.delete_dishes(dishes) do
      send_resp(conn, :no_content, "")
    end
  end
end

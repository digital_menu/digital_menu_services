defmodule DigitalMenuServicesWeb.SectionController do
  use DigitalMenuServicesWeb, :controller

  alias DigitalMenuServices.Menu
  alias DigitalMenuServices.Menu.Section

  action_fallback DigitalMenuServicesWeb.FallbackController

  def index(conn, %{"user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id}) do
    sections = Menu.list_sections(user_id, restaurant_id, menu_id)
    render(conn, "index.json", sections: sections)
  end

  def create(conn, %{"section" => section_params, "user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id}) do
    section_params = section_params
    |> Map.put("user_id", user_id)
    |> Map.put("restaurant_id", restaurant_id)
    |> Map.put("menu_id", menu_id)

    res = if Map.has_key?(section_params, "image") and section_params["image"] do
      {start, length} = :binary.match(section_params["image"], ";base64,")
      raw = :binary.part(section_params["image"], start + length, byte_size(section_params["image"]) - start - length)
      status = ExAws.S3.put_object("digital-menu-dev", "sections/"<>user_id<>restaurant_id<>menu_id<>section_params["name"]<>".png", Base.decode64!(raw))
      |> ExAws.request!()
      |> get_in([:status_code])
      case status do
        200 -> Map.put(section_params, "back_image", "https://digital-menu-dev.s3-us-west-2.amazonaws.com/sections/"<>user_id<>restaurant_id<>menu_id<>section_params["name"]<>".png")
        _ -> Map.put(section_params, "back_image", nil)
      end
    end
    IO.inspect("OFJEO")
    if res do
      with {:ok, %Section{} = section} <- Menu.create_section(res) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_restaurant_menu_section_path(conn, :show, user_id, restaurant_id, menu_id, section))
        |> render("show.json", section: section)
      end
    else
      with {:ok, %Section{} = section} <- Menu.create_section(section_params) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_restaurant_menu_section_path(conn, :show, user_id, restaurant_id, menu_id, section))
        |> render("show.json", section: section)
      end
    end
  end

  def show(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id}) do
    section = Menu.get_section!(id, user_id, restaurant_id, menu_id)
    render(conn, "show.json", section: section)
  end

  def update(conn, %{"id" => id, "section" => section_params, "user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id}) do
    section = Menu.get_section!(id, user_id, restaurant_id, menu_id)

    with {:ok, %Section{} = section} <- Menu.update_section(section, section_params) do
      render(conn, "show.json", section: section)
    end
  end

  def delete(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id, "menu_id" => menu_id}) do
    section = Menu.get_section!(id, user_id, restaurant_id, menu_id)

    with {:ok, %Section{}} <- Menu.delete_section(section) do
      send_resp(conn, :no_content, "")
    end
  end
end

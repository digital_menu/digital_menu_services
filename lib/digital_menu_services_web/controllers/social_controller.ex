defmodule DigitalMenuServicesWeb.SocialController do
  use DigitalMenuServicesWeb, :controller

  alias DigitalMenuServices.Credential
  alias DigitalMenuServices.Credential.Social

  action_fallback DigitalMenuServicesWeb.FallbackController

  def index(conn, _params) do
    socials = Credential.list_socials()
    render(conn, "index.json", socials: socials)
  end

  def create(conn, %{"social" => social_params}) do
    with {:ok, %Social{} = social} <- Credential.create_social(social_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.social_path(conn, :show, social))
      |> render("show.json", social: social)
    end
  end

  def show(conn, %{"id" => id}) do
    social = Credential.get_social!(id)
    render(conn, "show.json", social: social)
  end

  def update(conn, %{"id" => id, "social" => social_params}) do
    social = Credential.get_social!(id)

    with {:ok, %Social{} = social} <- Credential.update_social(social, social_params) do
      render(conn, "show.json", social: social)
    end
  end

  def delete(conn, %{"id" => id}) do
    social = Credential.get_social!(id)

    with {:ok, %Social{}} <- Credential.delete_social(social) do
      send_resp(conn, :no_content, "")
    end
  end
end

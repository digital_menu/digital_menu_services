defmodule DigitalMenuServicesWeb.UserController do
  use DigitalMenuServicesWeb, :controller

  alias DigitalMenuServices.Account
  alias DigitalMenuServices.Account.User

  action_fallback DigitalMenuServicesWeb.FallbackController

  def login(conn, %{"credentials" => credentials}) do
    with  user <- Account.get_login(credentials["username"]),
          true <- user && Bcrypt.verify_pass(credentials["password"], user.internal.password) do
      render(conn, "show.json", user: user)
    else
      _ -> {:error, :not_found}
    end
  end

  def index(conn, _params) do
    users = Account.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}) do
    res = if Map.has_key?(user_params, "profile_picture") and user_params["profile_picture"] != "" do
      {start, length} = :binary.match(user_params["profile_picture"], ";base64,")
      raw = :binary.part(user_params["profile_picture"], start + length, byte_size(user_params["profile_picture"]) - start - length)
      status = ExAws.S3.put_object("digital-menu-dev", "users/"<>user_params["username"]<>".png", Base.decode64!(raw))
      |> ExAws.request!()
      |> get_in([:status_code])
      case status do
        200 -> Map.put(user_params, "profile_picture", "https://digital-menu-dev.s3-us-west-2.amazonaws.com/users/"<>user_params["username"]<>".png")
        _ -> Map.put(user_params, "profile_picture", nil)
      end
    end

    if res do
      with {:ok, %User{} = user} <- Account.create_user(res) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_path(conn, :show, user))
        |> render("show.json", user: user)
      end
    else
      with {:ok, %User{} = user} <- Account.create_user(user_params) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_path(conn, :show, user))
        |> render("show.json", user: user)
      end
    end
  end

  def show(conn, %{"id" => id}) do
    user = Account.get_user!(id)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Account.get_user!(id)

    with {:ok, %User{} = user} <- Account.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Account.get_user!(id)

    with {:ok, %User{}} <- Account.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end

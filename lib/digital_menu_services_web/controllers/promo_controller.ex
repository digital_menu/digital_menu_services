defmodule DigitalMenuServicesWeb.PromoController do
  use DigitalMenuServicesWeb, :controller

  alias DigitalMenuServices.Restaurant
  alias DigitalMenuServices.Restaurant.Promo

  action_fallback DigitalMenuServicesWeb.FallbackController

  def index(conn, %{"user_id" => user_id, "restaurant_id" => restaurant_id}) do
    promos = Restaurant.list_promos(user_id, restaurant_id)
    render(conn, "index.json", promos: promos)
  end

  def create(conn, %{"promo" => promo_params, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    promo_params = promo_params
    |> Map.put("user_id", user_id)
    |> Map.put("restaurant_id", restaurant_id)

    with {:ok, %Promo{} = promo} <- Restaurant.create_promo(promo_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_restaurant_promo_path(conn, :show, user_id, restaurant_id, promo))
      |> render("show.json", promo: promo)
    end
  end

  def show(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    promo = Restaurant.get_promo!(id, user_id, restaurant_id)
    render(conn, "show.json", promo: promo)
  end

  def update(conn, %{"id" => id, "promo" => promo_params, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    promo = Restaurant.get_promo!(id, user_id, restaurant_id)

    with {:ok, %Promo{} = promo} <- Restaurant.update_promo(promo, promo_params) do
      render(conn, "show.json", promo: promo)
    end
  end

  def delete(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    promo = Restaurant.get_promo!(id, user_id, restaurant_id)

    with {:ok, %Promo{}} <- Restaurant.delete_promo(promo) do
      send_resp(conn, :no_content, "")
    end
  end
end

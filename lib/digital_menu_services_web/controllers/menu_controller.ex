defmodule DigitalMenuServicesWeb.MenuController do
  use DigitalMenuServicesWeb, :controller

  alias DigitalMenuServices.Account
  alias DigitalMenuServices.Restaurant
  alias DigitalMenuServices.Restaurant.Menu

  action_fallback DigitalMenuServicesWeb.FallbackController

  def get_menu(conn, %{"name" => name, "menu" => menu}) do
    restaurant = Account.get_restaurant_by_name!(name)
    menu = Restaurant.get_menu_by_name!(menu, restaurant.user_id, restaurant.id)
    render(conn, "public.json", menu: menu)
  end


  def index(conn, %{"user_id" => user_id, "restaurant_id" => restaurant_id}) do
    menus = Restaurant.list_menus(user_id, restaurant_id)
    render(conn, "index.json", menus: menus)
  end

  def create(conn, %{"menu" => menu_params, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    menu_params = menu_params
    |> Map.put("user_id", user_id)
    |> Map.put("restaurant_id", restaurant_id)

    res = if Map.has_key?(menu_params, "image") and menu_params["image"] do
      {start, length} = :binary.match(menu_params["image"], ";base64,")
      raw = :binary.part(menu_params["image"], start + length, byte_size(menu_params["image"]) - start - length)
      status = ExAws.S3.put_object("digital-menu-dev", "menus/"<>user_id<>restaurant_id<>menu_params["name"]<>".png", Base.decode64!(raw))
      |> ExAws.request!()
      |> get_in([:status_code])
      case status do
        200 -> Map.put(menu_params, "image", "https://digital-menu-dev.s3-us-west-2.amazonaws.com/menus/"<>user_id<>restaurant_id<>menu_params["name"]<>".png")
        _ -> Map.put(menu_params, "image", nil)
      end
    end

    if res do
      with {:ok, %Menu{} = menu} <- Restaurant.create_menu(res) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_restaurant_menu_path(conn, :show, user_id, restaurant_id, menu))
        |> render("show.json", menu: menu)
      end
    else
      with {:ok, %Menu{} = menu} <- Restaurant.create_menu(menu_params) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.user_restaurant_menu_path(conn, :show, user_id, restaurant_id, menu))
        |> render("show.json", menu: menu)
      end
    end
  end

  def show(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    menu = Restaurant.get_menu!(id, user_id, restaurant_id)
    render(conn, "show.json", menu: menu)
  end

  def update(conn, %{"id" => id, "menu" => menu_params, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    menu = Restaurant.get_menu!(id, user_id, restaurant_id)

    with {:ok, %Menu{} = menu} <- Restaurant.update_menu(menu, menu_params) do
      render(conn, "show.json", menu: menu)
    end
  end

  def delete(conn, %{"id" => id, "user_id" => user_id, "restaurant_id" => restaurant_id}) do
    menu = Restaurant.get_menu!(id, user_id, restaurant_id)

    with {:ok, %Menu{}} <- Restaurant.delete_menu(menu) do
      send_resp(conn, :no_content, "")
    end
  end
end

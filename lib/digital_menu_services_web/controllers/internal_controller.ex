defmodule DigitalMenuServicesWeb.InternalController do
  use DigitalMenuServicesWeb, :controller

  alias DigitalMenuServices.Credential
  alias DigitalMenuServices.Credential.Internal

  action_fallback DigitalMenuServicesWeb.FallbackController

  def index(conn, _params) do
    internals = Credential.list_internals()
    render(conn, "index.json", internals: internals)
  end

  def create(conn, %{"internal" => internal_params}) do
    with {:ok, %Internal{} = internal} <- Credential.create_internal(internal_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.internal_path(conn, :show, internal))
      |> render("show.json", internal: internal)
    end
  end

  def show(conn, %{"id" => id}) do
    internal = Credential.get_internal!(id)
    render(conn, "show.json", internal: internal)
  end

  def update(conn, %{"id" => id, "internal" => internal_params}) do
    internal = Credential.get_internal!(id)

    with {:ok, %Internal{} = internal} <- Credential.update_internal(internal, internal_params) do
      render(conn, "show.json", internal: internal)
    end
  end

  def delete(conn, %{"id" => id}) do
    internal = Credential.get_internal!(id)

    with {:ok, %Internal{}} <- Credential.delete_internal(internal) do
      send_resp(conn, :no_content, "")
    end
  end
end

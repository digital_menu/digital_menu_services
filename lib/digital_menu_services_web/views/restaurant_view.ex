defmodule DigitalMenuServicesWeb.RestaurantView do
  use DigitalMenuServicesWeb, :view
  alias DigitalMenuServicesWeb.{RestaurantView, MenuView, PromoView, SettingView}

  def render("index.json", %{restaurants: restaurants}) do
    %{data: render_many(restaurants, RestaurantView, "restaurant.json")}
  end

  def render("show.json", %{restaurant: restaurant}) do
    %{data: render_one(restaurant, RestaurantView, "restaurant.json")}
  end

  def render("restaurant.json", %{restaurant: restaurant}) do
    IO.inspect(restaurant)
    menus = if Ecto.assoc_loaded?(restaurant.menus) do
      render_many(restaurant.menus, MenuView, "menu.json")
    end
    setting = if Ecto.assoc_loaded?(restaurant.setting) do
      render_one(restaurant.setting, SettingView, "setting.json")
    end
    %{
      id: restaurant.id,
      name: restaurant.name,
      image: restaurant.image,
      description: restaurant.description,
      address: restaurant.address,
      coordinates: restaurant.coordinates,
      enabled: restaurant.enabled,
      setting: setting,
      menus: menus
    }
  end

  def render("public.json", %{restaurant: restaurant}) do
    menus = if Ecto.assoc_loaded?(restaurant.menus) do
      render_many(restaurant.menus, MenuView, "public.json")
    end
    promos = if Ecto.assoc_loaded?(restaurant.promos) do
      render_many(restaurant.promos, PromoView, "public.json")
    end
    setting = if Ecto.assoc_loaded?(restaurant.setting) do
      render_one(restaurant.setting, SettingView, "public.json")
    end
    %{
      name: restaurant.name,
      image: restaurant.image,
      description: restaurant.description,
      address: restaurant.address,
      coordinates: restaurant.coordinates,
      enabled: restaurant.enabled,
      setting: setting,
      promos: promos,
      menus: menus
    }
  end
end

defmodule DigitalMenuServicesWeb.DishesView do
  use DigitalMenuServicesWeb, :view
  alias DigitalMenuServicesWeb.DishesView

  def render("index.json", %{dishes: dishes}) do
    %{data: render_many(dishes, DishesView, "dishes.json")}
  end

  def render("show.json", %{dishes: dishes}) do
    %{data: render_one(dishes, DishesView, "dishes.json")}
  end

  def render("dishes.json", %{dishes: dishes}) do
    %{id: dishes.id,
      name: dishes.name,
      description: dishes.description,
      image: dishes.image,
      price: dishes.price,
      enabled: dishes.enabled,
      extras: dishes.extras}
  end

  def render("public.json", %{dishes: dishes}) do
    %{
      name: dishes.name,
      description: dishes.description,
      image: dishes.image,
      price: dishes.price,
      enabled: dishes.enabled,
      extras: dishes.extras}
  end
end

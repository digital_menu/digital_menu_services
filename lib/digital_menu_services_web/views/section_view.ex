defmodule DigitalMenuServicesWeb.SectionView do
  use DigitalMenuServicesWeb, :view
  alias DigitalMenuServicesWeb.{ SectionView, DishesView }

  def render("index.json", %{sections: sections}) do
    %{data: render_many(sections, SectionView, "section.json")}
  end

  def render("show.json", %{section: section}) do
    %{data: render_one(section, SectionView, "section.json")}
  end

  def render("section.json", %{section: section}) do
    %{id: section.id,
      name: section.name,
      description: section.description,
      back_image: section.back_image}
  end

  def render("public.json", %{section: section}) do
    dishes = if Ecto.assoc_loaded?(section.dishes) do
      render_many(section.dishes, DishesView, "public.json")
    end
    %{
      name: section.name,
      description: section.description,
      back_image: section.back_image,
      dishes: dishes
    }
  end
end

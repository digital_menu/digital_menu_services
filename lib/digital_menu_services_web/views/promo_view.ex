defmodule DigitalMenuServicesWeb.PromoView do
  use DigitalMenuServicesWeb, :view
  alias DigitalMenuServicesWeb.PromoView

  def render("index.json", %{promos: promos}) do
    %{data: render_many(promos, PromoView, "promo.json")}
  end

  def render("show.json", %{promo: promo}) do
    %{data: render_one(promo, PromoView, "promo.json")}
  end

  def render("promo.json", %{promo: promo}) do
    %{id: promo.id,
      name: promo.name,
      description: promo.description,
      type: promo.type,
      image: promo.image,
      begin_date: promo.begin_date,
      end_date: promo.end_date}
  end

  def render("public.json", %{promo: promo}) do
    %{
      name: promo.name,
      description: promo.description,
      type: promo.type,
      image: promo.image,
      begin_date: promo.begin_date,
      end_date: promo.end_date}
  end
end

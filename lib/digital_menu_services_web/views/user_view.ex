defmodule DigitalMenuServicesWeb.UserView do
  use DigitalMenuServicesWeb, :view
  alias DigitalMenuServicesWeb.{ UserView, InternalView }

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    internal = if Ecto.assoc_loaded?(user.internal) do
      render_one(user.internal, InternalView, "internal.json")
    end
    %{
      id: user.id,
      username: user.username,
      email: user.email,
      profile_picture: user.profile_picture,
      internal: internal
    }
  end
end

defmodule DigitalMenuServicesWeb.SettingView do
  use DigitalMenuServicesWeb, :view
  alias DigitalMenuServicesWeb.SettingView

  def render("index.json", %{settings: settings}) do
    %{data: render_many(settings, SettingView, "setting.json")}
  end

  def render("show.json", %{setting: setting}) do
    %{data: render_one(setting, SettingView, "setting.json")}
  end

  def render("setting.json", %{setting: setting}) do
    %{id: setting.id,
      main_color: setting.main_color,
      secondary_color: setting.secondary_color,
      secondary_text_color: setting.secondary_text_color,
      text_color: setting.text_color,
      promo_timer: setting.promo_timer,
      font_type: setting.font_type}
  end

  def render("public.json", %{setting: setting}) do
    %{
      main_color: setting.main_color,
      secondary_color: setting.secondary_color,
      text_color: setting.text_color,
      secondary_text_color: setting.secondary_text_color,
      promo_timer: setting.promo_timer,
      font_type: setting.font_type}
  end
end

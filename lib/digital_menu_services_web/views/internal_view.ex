defmodule DigitalMenuServicesWeb.InternalView do
  use DigitalMenuServicesWeb, :view
  alias DigitalMenuServicesWeb.InternalView

  def render("index.json", %{internals: internals}) do
    %{data: render_many(internals, InternalView, "internal.json")}
  end

  def render("show.json", %{internal: internal}) do
    %{data: render_one(internal, InternalView, "internal.json")}
  end

  def render("internal.json", %{internal: internal}) do
    %{
      id: internal.id,
      type: internal.type
    }
  end
end

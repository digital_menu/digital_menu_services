defmodule DigitalMenuServicesWeb.MenuView do
  use DigitalMenuServicesWeb, :view
  alias DigitalMenuServicesWeb.{ MenuView, SectionView }

  def render("index.json", %{menus: menus}) do
    %{data: render_many(menus, MenuView, "menu.json")}
  end

  def render("show.json", %{menu: menu}) do
    %{data: render_one(menu, MenuView, "menu.json")}
  end

  def render("menu.json", %{menu: menu}) do
    sections = if Ecto.assoc_loaded?(menu.sections) do
      render_many(menu.sections, SectionView, "section.json")
    end
    %{id: menu.id,
      name: menu.name,
      type: menu.type,
      image: menu.image,
      schedule: menu.schedule,
      sections: sections
    }
  end

  def render("public.json", %{menu: menu}) do
    sections = if Ecto.assoc_loaded?(menu.sections) do
      render_many(menu.sections, SectionView, "public.json")
    end
    %{
      name: menu.name,
      type: menu.type,
      image: menu.image,
      schedule: menu.schedule,
      sections: sections
    }
  end
end

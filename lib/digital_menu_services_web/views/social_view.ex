defmodule DigitalMenuServicesWeb.SocialView do
  use DigitalMenuServicesWeb, :view
  alias DigitalMenuServicesWeb.SocialView

  def render("index.json", %{socials: socials}) do
    %{data: render_many(socials, SocialView, "social.json")}
  end

  def render("show.json", %{social: social}) do
    %{data: render_one(social, SocialView, "social.json")}
  end

  def render("social.json", %{social: social}) do
    %{id: social.id,
      access_token: social.access_token,
      refresh_token: social.refresh_token,
      type: social.type,
      social_media: social.social_media}
  end
end

defmodule DigitalMenuServicesWeb.Router do
  use DigitalMenuServicesWeb, :router

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
  end

  scope "/api", DigitalMenuServicesWeb do
    pipe_through :api

    post    "/login", UserController, :login
    options "/login", UserController, :options

    get     "/digital-menu/:name", RestaurantController, :get_restaurant do
      get     "/digital-menu/:name/:menu", MenuController, :get_menu
      options "/digital-menu/:name/:menu", MenuController, :options
    end
    options "/digital-menu/:name", RestaurantController, :options

    resources "/users", UserController, except: [:new, :edit] do
      resources "/socials", SocialController, except: [:new, :edit]
      resources "/internals", InternalController, except: [:new, :edit]

      resources "/restaurants", RestaurantController, except: [:new, :edit] do

        resources "/settings", SettingController, except: [:new, :edit]
        options   "/settings", SettingController, :options
        options   "/settings/:id", SettingController, :options

        resources "/promos", PromoController, except: [:new, :edit]
        options   "/promos", PromoController, :options
        options   "/promos/:id", PromoController, :options

        resources "/menus", MenuController, except: [:new, :edit] do

          resources "/sections", SectionController, except: [:new, :edit] do
            resources "/dishes",   DishesController, except: [:new, :edit]
            options "/dishes",     DishesController, :options
            options "/dishes/:id", DishesController, :options
          end
          options "/sections",     SectionController, :options
          options "/sections/:id", SectionController, :options
        end
        options "/menus",     MenuController, :options
        options "/menus/:id", MenuController, :options
      end
      options "/restaurants",     RestaurantController, :options
      options "/restaurants/:id", RestaurantController, :options
    end
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: DigitalMenuServicesWeb.Telemetry
    end
  end
end

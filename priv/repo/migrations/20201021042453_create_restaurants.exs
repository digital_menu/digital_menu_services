defmodule DigitalMenuServices.Repo.Migrations.CreateRestaurants do
  use Ecto.Migration

  def change do
    create table(:restaurants) do
      add :name, :string
      add :image, :string
      add :description, :string
      add :address, :string
      add :coordinates, :string
      add :enabled, :boolean

      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
    create unique_index(:restaurants, [:name])
  end
end

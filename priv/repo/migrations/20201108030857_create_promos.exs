defmodule DigitalMenuServices.Repo.Migrations.CreatePromos do
  use Ecto.Migration

  def change do
    create table(:promos) do
      add :name, :string
      add :description, :string
      add :type, :string
      add :image, :string
      add :begin_date, :string
      add :end_date, :string
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :restaurant_id, references(:restaurants, on_delete: :delete_all), null: false

      timestamps()
    end

  end
end

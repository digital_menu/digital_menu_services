defmodule DigitalMenuServices.Repo.Migrations.CreateSocials do
  use Ecto.Migration

  def change do
    create table(:socials) do
      add :access_token, :string
      add :refresh_token, :string
      add :type, :string
      add :social_media, :string
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:socials, [:user_id])
  end
end

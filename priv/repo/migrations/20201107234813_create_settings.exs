defmodule DigitalMenuServices.Repo.Migrations.CreateSettings do
  use Ecto.Migration

  def change do
    create table(:settings) do
      add :main_color, :string
      add :secondary_color, :string
      add :text_color, :string
      add :secondary_text_color, :string
      add :promo_timer, :integer
      add :font_type, :string
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :restaurant_id, references(:restaurants, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:settings, [:restaurant_id])
  end
end

defmodule DigitalMenuServices.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :username, :string
      add :email, :string
      add :profile_picture, :string

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end

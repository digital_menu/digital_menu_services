defmodule DigitalMenuServices.Repo.Migrations.CreateInternals do
  use Ecto.Migration

  def change do
    create table(:internals) do
      add :password, :string
      add :type, :string
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:internals, [:user_id])
  end
end

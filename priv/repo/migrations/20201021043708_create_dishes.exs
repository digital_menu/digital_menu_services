defmodule DigitalMenuServices.Repo.Migrations.CreateDishes do
  use Ecto.Migration

  def change do
    create table(:dishes) do
      add :name, :string
      add :description, :string
      add :image, :string
      add :price, :string
      add :enabled, :boolean
      add :extras, {:array, :string}
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :restaurant_id, references(:restaurants, on_delete: :delete_all), null: false
      add :menu_id, references(:menus, on_delete: :delete_all), null: false
      add :section_id, references(:sections, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:dishes, [:section_id])
  end
end

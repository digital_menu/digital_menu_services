defmodule DigitalMenuServices.Repo.Migrations.CreateSections do
  use Ecto.Migration

  def change do
    create table(:sections) do
      add :name, :string
      add :description, :string
      add :back_image, :string
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :restaurant_id, references(:restaurants, on_delete: :delete_all), null: false
      add :menu_id, references(:menus, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:sections, [:menu_id])
  end
end

defmodule DigitalMenuServices.Repo.Migrations.CreateMenus do
  use Ecto.Migration

  def change do
    create table(:menus) do
      add :name, :string
      add :type, :string
      add :image, :string
      add :schedule, {:array, :string}
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :restaurant_id, references(:restaurants, on_delete: :delete_all), null: false

      timestamps()
    end

  end
end
